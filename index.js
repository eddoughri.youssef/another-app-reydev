const express = require("express");
const app = express();
const port = 4000;
const token =
  "MTAzMTI4Mzk0NzMxMDY4MjEzMg.GIfBTX.CiAHSohSx8XSpFwCZ7yVxw4Qfmbaw0RnHpJiB4";
// This is an important value, even tho the bot will not have permissions to do anything but
//generate invites, it should be protected.
const inviteChannelId = "1030493542419795988";
// Discord ID of the discord channel ID, the channel where the invites are getting generated from.
const maxAge = 3600;
// Age of the invite link before it expires in seconds
const guildId = "1026787691758628864";
//guild ID of the discord server, serve to checks if members are already in the server

const Discord = require("discord.js");

app.get("/", async (req, res) => {
  

  try {
    const client = new Discord.Client({
      partials: ["MESSAGE", "CHANNEL", "REACTION"],
      intents: 98303,
    });
    await client.login(token);

    client.on("ready", async () => {
      console.log(
        `${client.user.username} is Online ` + ` \nID: ${client.user.id}`
      );

      await client.channels.cache
        .get(inviteChannelId)
        .createInvite({
            maxAge:3600,
            maxUsers:1,
            unique:true,
            reason:"ECO Gateway"
        })
        .then((invite) => {
          res.send(invite.toString());
        });
    });
  } catch (err) {
    res.send(err);
  }
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
